<h1>Student Management System</h1>
Developed in <code>C++</code>
<br>
<br>

<h3>Hep Table</h3>
<hr>
Here is list of commands:<br>
1 : Adding New Student<br>
2 : Adding New Passed Course<br>
3 : Adding New Course<br>
4 : Sorting Students By Lastname<br>
5 : Sorting Students By Student Number<br>
6 : List Of Failed Students<br>
7 : List Of Best Students<br>
8 : List Of Students With Less Than 14 Units Passed<br>
9 : Removing Students<br>
10: Showing Student's Info<br>
11: Removing Course<br>
12: Exit

You can also get help table by this command:
<pre>
outputfile --help
</pre>


<h3>Files As Tables</h3>
<hr>
There are 3 files in this project that used like tables:
<ul>
<li>Students.txt</li>
<li>Courses.txt</li>
<li>Passed.txt</li>
</ul>
Notice: There is also "Temp.txt", but it's a temperory file and program use it in removing courses and students.

Structure of files are shown below:
1. Students
<pre>
Firstname , Lastame , Student Number 
</pre>
2. Courses
<pre>
Code , Name , Unit , Teacher
</pre>
3. Passed
<pre>
Student Number , Course Code , Grade
</pre>

<h3>List of Functions<h3>
<hr>

Main Function:
```cpp
void DoCommand(int Command)
```
Others:
```cpp
void AddStudent();
void AddPassedCourse();
void AddCourse();            
void StudentOrderByLastname();
void StudentOrderByNumber();
void FaildStudents();
void StudentLess14();
void RemoveStudent();
void ShowStudentInfo();
void RemoveCourse();
```
List of sub-functions:

```cpp
int SearchStudent(int SNumber) // # SearchStudent - If Student Exists, Returns Line Number, Otherwise, Returns -1
int SearchCourse(int CNumber) // # SearchCourse - If Course Exists, Returns Line Number, Otherwise, Returns -1
void ShowCourseNameByID(int ID)
int GetUnitByCourseID(int ID)
int TotalPassedUnitsByStudentID(int ID)
float AvgByStudentID(int ID)
```


