#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>

#define StudentFilePath "Students.txt"
#define CourseFilePath "Courses.txt"
#define TempFilePath "Temp.txt"
#define PassedCourseFilePath "Passed.txt"

using namespace std;
//////////////////////////////////////////////////////////////////////
// Defining Structs
//////////////////////////////////////////////////////////////////////
// # Course
typedef struct course {
    char Code[8];
    char Name[19];
    int Uint;
    char Teacher[21];
} Course;

// # Student
typedef struct student {
    char Firstname[30];
    char Lastname[30];
    char StudentNumber[7];  
    char Avg[10];  
} Student;
/////////////////////////// End of Defining Structs ////////////////////


//////////////////////////////////////////////////////////////////////
// Defining SubCommands Functions
//////////////////////////////////////////////////////////////////////
// # AddStudent
void AddStudent() {
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"a");
    char Firstname[30];
    char Lastname[30];
    char StudentNumber[7];  
    cout << ">Firstname: ";
    cin >> Firstname;
    cout << ">Lastname: ";    
    cin >> Lastname;    
    cout << ">Student Number: ";    
    cin >> StudentNumber;

    fprintf(StuFile,Firstname);
    fprintf(StuFile," , ");    
    fprintf(StuFile,Lastname);
    fprintf(StuFile," , ");        
    fprintf(StuFile,StudentNumber);
    fprintf(StuFile,"\n");
    fclose(StuFile);
    cout << "> New student added sucssefully" << endl;    

}


// # AddCourse
void AddCourse() {
    FILE *CouFile;
    CouFile = fopen(CourseFilePath,"a");
    char Code[8];
    char Name[19];
    int Unit;
    char Teacher[21];  
    cout << ">Code: ";
    cin >> Code;
    cout << ">Name: ";    
    cin >> Name;    
    cout << ">Unit: ";    
    cin >> Unit;
    cout << ">Teacher: ";    
    cin >> Teacher;
    

    fprintf(CouFile,Code);
    fprintf(CouFile," , ");    
    fprintf(CouFile,Name);
    fprintf(CouFile," , ");        
    fprintf(CouFile,"%d",Unit);
    fprintf(CouFile," , ");        
    fprintf(CouFile,Teacher);    
    fprintf(CouFile,"\n");

    cout << "> New course added sucssefully" << endl;    
    fclose(CouFile);

}


// # SearchStudent - If Student Exists, Returns Line Number, Otherwise, Returns -1
int SearchStudent(int SNumber)
{
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");
    if(!StuFile)
    {
        cout << "Error in opening file!" << endl;
        return -1;
    }

    char Firstname[30];
    char Lastname[30];
    char StudentNumber[8];
    int StuNum;    

    int LineNumber=-1;
    int LineCounter = 1;
    char Buff[70];
    while(fgets(Buff,69,StuFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %s",Firstname,Lastname,StudentNumber);
        StuNum = atoi(StudentNumber);

        if(StuNum == SNumber)
        {
            LineNumber = LineCounter;
        }
        LineCounter++;
    }
    fclose(StuFile);
    return LineNumber;
}


// # SearchCourse - If Course Exists, Returns Line Number, Otherwise, Returns -1
int SearchCourse(int CNumber)
{
    FILE *CouFile;
    CouFile = fopen(CourseFilePath,"r");
    if(!CouFile)
    {
        cout << "Error in opening file!" << endl;
        return -1;
    }

    char Code[8];
    char Name[19];
    int Unit;
    char Teacher[21];
    int CourseCode;    

    int LineNumber=-1;
    int LineCounter = 1;
    char Buff[70];
    while(fgets(Buff,69,CouFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %d , %s",Code,Name,&Unit,Teacher);
        CourseCode = atoi(Code);

        if(CourseCode == CNumber)
        {
            LineNumber = LineCounter;
            cout << LineNumber << endl;
        }
        LineCounter++;
    }
    fclose(CouFile);
    return LineNumber;
}

// # RemoveStudent
void RemoveStudent()
{
    /// Opening File    
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");
    if(!StuFile)
    {
        cout << "Error in opening file!" << endl;
        return;
    }

    FILE *TFile;
    TFile = fopen(TempFilePath,"w");
    if(!TFile)
    {
        cout << "Error in opening file!" << endl;
        return;
    }
    // Getting Student Number 
    cout << ">Enter Student Number: ";
    int StuNum;
    cin >> StuNum;
    int StudentLineNumber = SearchStudent(StuNum);
    int LineNumber=0;
    char Buff[70];
    while(fgets(Buff,69,StuFile) != NULL)
    {
        LineNumber++;        
        if(StudentLineNumber != LineNumber)
        {
            fprintf(TFile,"%s",Buff);                
        }
    }
    fclose(StuFile);
    fclose(TFile);
    remove(StudentFilePath);
    rename(TempFilePath,StudentFilePath);

    cout << "Student removed successfully." << endl;
    
}


// # RemoveCourse
void RemoveCourse()
{
    /// Opening File    
    FILE *CouFile;
    CouFile = fopen(CourseFilePath,"r");
    if(!CouFile)
    {
        cout << "Error in opening file!" << endl;
        return;
    }

    FILE *TFile;
    TFile = fopen(TempFilePath,"w");
    if(!TFile)
    {
        cout << "Error in opening file!" << endl;
        return;
    }
    // Getting Course Code 
    cout << ">Enter Course Code: ";
    int CouCode;
    cin >> CouCode;
    int CourseLineNumber = SearchCourse(CouCode);
    int LineNumber=0;
    char Buff[70];
    while(fgets(Buff,69,CouFile) != NULL)
    {
        if(CourseLineNumber != LineNumber)
        {
            fprintf(TFile,"%s",Buff);                
        }
        LineNumber++;                
    }
    fclose(CouFile);
    fclose(TFile);
    remove(CourseFilePath);
    rename(TempFilePath,CourseFilePath);
    
}


// # StudentOrderByNumber

void StudentOrderByNumber()
{
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");

    int Count=0;
    char Buff[70];
    while(fgets(Buff,70,StuFile) != NULL)
    {
        Count++;
    }
    fclose(StuFile);
    StuFile = NULL;
    //////////////

    Student* Stu = NULL;
    Stu = (Student*)calloc(Count,sizeof(Student));
    //// Reading File and Assigning to Struct
    StuFile = fopen(StudentFilePath,"r");
    int i =0;
    while(fgets(Buff,70,StuFile) != NULL)
    {
        sscanf(Buff,("%s , %s , %s"),Stu[i].Firstname,Stu[i].Lastname,Stu[i].StudentNumber);
        i++;
    }

    // Sorting
    int j;
    char * temp;
    for(i=0; i<Count;i++)
    {
        for(j=0;j<Count-i;j++)
        {
            if(strcmp(Stu[j].StudentNumber,Stu[j+1].StudentNumber)>0)
            {
                strcpy(temp,Stu[j].StudentNumber);
                strcpy(Stu[j].StudentNumber,Stu[j+1].StudentNumber);
                strcpy(Stu[j+1].StudentNumber,temp);

                strcpy(temp,Stu[j].Firstname);
                strcpy(Stu[j].Firstname,Stu[j+1].Firstname);
                strcpy(Stu[j+1].Firstname,temp);
                
                strcpy(temp,Stu[j].Lastname);
                strcpy(Stu[j].Lastname,Stu[j+1].Lastname);
                strcpy(Stu[j+1].Lastname,temp);
            }
        }
    }

    // Printing

    for(i=0;i<Count;i++)
    {
        cout << Stu[i].Firstname << " " << Stu[i].Lastname << " - " << Stu[i].StudentNumber << endl;
    }

}

// # StudentOrderBylastname
void StudentOrderByLastname()
{
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");

    int Count=0;
    char Buff[70];
    while(fgets(Buff,70,StuFile) != NULL)
    {
        Count++;
    }
    fclose(StuFile);
    StuFile = NULL;
    //////////////

    Student* Stu = NULL;
    Stu = (Student*)calloc(Count,sizeof(Student));
    //// Reading File and Assigning to Struct
    StuFile = fopen(StudentFilePath,"r");
    int i =0;
    while(fgets(Buff,70,StuFile) != NULL)
    {
        sscanf(Buff,("%s , %s , %s"),Stu[i].Firstname,Stu[i].Lastname,Stu[i].StudentNumber);
        i++;
    }

    // Sorting
    int j;
    char * temp;
    for(i=0; i<Count;i++)
    {
        for(j=0;j<Count-i;j++)
        {
            if(strcmp(Stu[j].Lastname,Stu[j+1].Lastname)>0)
            {
                strcpy(temp,Stu[j].StudentNumber);
                strcpy(Stu[j].StudentNumber,Stu[j+1].StudentNumber);
                strcpy(Stu[j+1].StudentNumber,temp);

                strcpy(temp,Stu[j].Firstname);
                strcpy(Stu[j].Firstname,Stu[j+1].Firstname);
                strcpy(Stu[j+1].Firstname,temp);
                
                strcpy(temp,Stu[j].Lastname);
                strcpy(Stu[j].Lastname,Stu[j+1].Lastname);
                strcpy(Stu[j+1].Lastname,temp);
            }
        }
    }

    // Printing

    for(i=0;i<Count;i++)
    {
        cout << Stu[i].Firstname << " " << Stu[i].Lastname << " - " << Stu[i].StudentNumber << endl;
    }

}

// # AddPassedCourse
void AddPassedCourse()
{
    FILE *PassedFile;
    PassedFile = fopen(PassedCourseFilePath,"a");
    if(!PassedFile)
    {
        cout << ">Faild to open file";
        return;
    }
    ////
    char StudentNumber[8];
    char CourseNumber[8];
    int Grade;
    
    cout << ">Enter Student Number: ";
    cin >> StudentNumber;

    cout << ">Enter Course Number: ";
    cin >> CourseNumber;

    cout << ">Enter Grade: ";
    cin >> Grade;
    ////
    fprintf(PassedFile,StudentNumber);
    fprintf(PassedFile," , ");
    fprintf(PassedFile,CourseNumber);
    fprintf(PassedFile," , ");
    fprintf(PassedFile,"%d",Grade);
    fprintf(PassedFile,"\n");
    
    fclose(PassedFile);

    cout << ">New Grade Added." << endl;
}


// # ShowCourseNameByID

void ShowCourseNameByID(int ID)
{
    FILE *CouFile;
    CouFile = fopen(CourseFilePath,"r");
    if(!CouFile)
    {
        cout << ">Failed to open file!" << endl;
        return;
    }
    ///
    char CourseNumber[8];
    char CourseName[20];
    char Teacher[20];
    int Units;    
    char Buff[70];
    while(fgets(Buff,70,CouFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %d , %s",CourseNumber,CourseName,&Units,Teacher);
        if(atoi(CourseNumber) == ID)
        {
            cout << CourseName;
            break;
        }
    }
    fclose(CouFile);
}

// # GetUnitByCourseID

int GetUnitByCourseID(int ID)
{
    FILE *CouFile;
    CouFile = fopen(CourseFilePath,"r");
    if(!CouFile)
    {
        cout << ">Failed to open file!" << endl;
        return 0;
    }
    //
    char Buff[70];
    char CourseName[20];
    char CourseNumber[8];
    int Units;
    char Teacher[20];
    //
    while(fgets(Buff,70,CouFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %d , %s",CourseNumber,CourseName,&Units,Teacher);
        if(atoi(CourseNumber) == ID)
        {
            return Units;
        }
    }
    return 0;
}

// # TotalPassedUnitsByStudentID

int TotalPassedUnitsByStudentID(int ID)
{
    FILE *PassedFile;
    PassedFile = fopen(PassedCourseFilePath,"r");
    if(!PassedFile)
    {
        cout << ">Failed to open file!" << endl;
        return 0;
    }
    //
    char Buff[70];
    char StudentNumber[8];
    char CourseNumber[8];
    int Grade;
    int Total = 0;
    //
    while(fgets(Buff,70,PassedFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %d",StudentNumber,CourseNumber,&Grade);
        if(atoi(StudentNumber) == ID)
        {
            Total += GetUnitByCourseID(atoi(CourseNumber));        
        }
    }
    return Total;
}

// # AvgByStudentID

float AvgByStudentID(int ID)
{
    FILE *PassedFile;
    PassedFile = fopen(PassedCourseFilePath,"r");
    if(!PassedFile)
    {
        cout << "Failed to open file!" << endl;
        return 0;
    }
    //
    char StudentNumber[8];
    char CourseNumber[8];
    int Grade;
    char Buff[70];
    int Total =0;
    while(fgets(Buff,70,PassedFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %d",StudentNumber,CourseNumber,&Grade);
        if(atoi(StudentNumber) == ID)
        {
            Total += Grade * GetUnitByCourseID(atoi(CourseNumber));
        }
    }
    float Avg = (float) Total / TotalPassedUnitsByStudentID(ID);
    return Avg;
}

// # ShowStudentInfo

void ShowStudentInfo()
{
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");
    if(!StuFile)
    {
        cout << "Failed to open file!" << endl;
        return;
    }
    ///
    char StudentNumber[8];
    char Firstname[30];
    char Lastname[30];    
    cout << ">Enter Student Number: ";
    cin >> StudentNumber;
    ///
    char Buff[70];
    if(SearchStudent(atoi(StudentNumber)) == -1)
    {
        cout << ">Student not found!" << endl;
    }
    else
    {
        int LineNumber=1;
        while(fgets(Buff,70,StuFile) != NULL)
        {
            if(SearchStudent(atoi(StudentNumber)) != LineNumber)
            {
                LineNumber++;
            }
            else
            {
                sscanf(Buff,"%s , %s , %s",Firstname,Lastname,StudentNumber);
                break;
            }
        }

        FILE *PassedFile;
        PassedFile = fopen(PassedCourseFilePath,"r");
        if(!PassedFile)
        {
            cout << ">Failed to open file!" << endl;
            return;
        }
        ///

        cout << ">Student's Info: " << endl;
        cout << ">Firstname: " << Firstname << endl;
        cout << ">Lastname: " << Lastname << endl;
        cout << ">Student Number: " << StudentNumber << endl;
        cout << ">Passed Courses: " << endl;

        char StuNum[8];
        char CourseNum[8];
        int Grade;
        
        while(fgets(Buff,70,PassedFile) != NULL)
        {
            sscanf(Buff,"%s , %s , %d",StuNum,CourseNum,&Grade);
            if(strcmp(StuNum,StudentNumber) == 0)
            {
                cout << "Course Number: " << CourseNum << " - Name: "; 
                ShowCourseNameByID(atoi(CourseNum));
                cout <<  " - Grade: " << Grade << endl;
            }
        }

        //// Passed Units and Average
        cout << ">Total Passed Units: " << TotalPassedUnitsByStudentID(atoi(StudentNumber)) << endl;
        cout << ">Average of Grades: " << AvgByStudentID(atoi(StudentNumber)) << endl;
        

        
    }
    fclose(StuFile);
}

// # StudentLess14

void StudentLess14()
{
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");
    if(!StuFile)
    {
        cout << "Failed to open file!" << endl;
        return;
    }
    ///
    char StudentNumber[8];
    char Firstname[30];
    char Lastname[30];
    char Buff[70];
    int Count=0;
    //
    cout << ">List of students with less than 14 units passed: " << endl;
    while(fgets(Buff,70,StuFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %s",Firstname,Lastname,StudentNumber);
        if(TotalPassedUnitsByStudentID(atoi(StudentNumber)) < 14)
        {
            cout << "Name: " << Firstname << " " << Lastname << " - " << StudentNumber << endl;
            Count++;
        }        
    }
    if(Count == 0)
        cout << "No student found!" << endl;
    fclose(StuFile);    
}

// # StudentLess14

void FaildStudents()
{
    FILE *StuFile;
    StuFile = fopen(StudentFilePath,"r");
    if(!StuFile)
    {
        cout << "Failed to open file!" << endl;
        return;
    }
    ///
    char StudentNumber[8];
    char Firstname[30];
    char Lastname[30];
    char Buff[70];
    int Count=0;
    //
    cout << ">List of failed students: " << endl;
    while(fgets(Buff,70,StuFile) != NULL)
    {
        sscanf(Buff,"%s , %s , %s",Firstname,Lastname,StudentNumber);
        if(AvgByStudentID(atoi(StudentNumber)) < 12)
        {
            cout << "Name: " << Firstname << " " << Lastname << " - " << StudentNumber << "  - Avg: " << AvgByStudentID(atoi(StudentNumber)) << endl;
            Count++;
        }        
    }
    if(Count == 0)
        cout << "No student found!" << endl;
    fclose(StuFile);
}

// # BestStudents

// void BestStudents()
// {
//     FILE *StuFile;
//     StuFile = fopen(StudentFilePath,"r");

//     int Count=0;
//     char Buff[70];
//     while(fgets(Buff,70,StuFile) != NULL)
//     {
//         Count++;
//     }
//     fclose(StuFile);
//     StuFile = NULL;
//     //////////////

//     Student* Stu = NULL;
//     Stu = (Student*)calloc(Count,sizeof(Student));
//     //// Reading File and Assigning to Struct
//     StuFile = fopen(StudentFilePath,"r");
//     int i =0;
//     while(fgets(Buff,70,StuFile) != NULL)
//     {
//         sscanf(Buff,("%s , %s , %s"),Stu[i].Firstname,Stu[i].Lastname,Stu[i].StudentNumber);
//         sprintf(Stu[i].Avg,"%f",AvgByStudentID(atoi(Stu[i].StudentNumber)));
        
//         i++;
//     }

//     // Sorting By Avg
//     int j;
//     float TAvg;
//     char * temp;
//     for(i=0; i<Count;i++)
//     {
//         for(j=0;j<Count-i;j++)
//         {
//             if(strcmp(Stu[j].Avg,Stu[j+1].Avg)<0)
//             {
//                 strcpy(temp,Stu[j].StudentNumber);
//                 strcpy(Stu[j].StudentNumber,Stu[j+1].StudentNumber);
//                 strcpy(Stu[j+1].StudentNumber,temp);

//                 strcpy(temp,Stu[j].Firstname);
//                 strcpy(Stu[j].Firstname,Stu[j+1].Firstname);
//                 strcpy(Stu[j+1].Firstname,temp);
                
//                 strcpy(temp,Stu[j].Lastname);
//                 strcpy(Stu[j].Lastname,Stu[j+1].Lastname);
//                 strcpy(Stu[j+1].Lastname,temp);

//                 strcpy(temp,Stu[j].Avg);
//                 strcpy(Stu[j].Avg,Stu[j+1].Avg);
//                 strcpy(Stu[j+1].Avg,temp);

//                 // TAvg = Stu[j].Avg;
//                 // Stu[j].Avg = Stu[j+1].Avg;
//                 // Stu[j+1].Avg = TAvg;
//             }
//         }
//     }

//     // Printing
//     for(i=0;i<Count;i++)
//     {
//         cout << Stu[i].Firstname << " " << Stu[i].Lastname << " - " << Stu[i].StudentNumber << " : " <<Stu[i].Avg << endl;
//     }
//     fclose(StuFile);
// }

/////////////////////////// End of SubCommands Functions ////////////////////

//////////////////////////////////////////////////////////////////////
// Defining Commands Function
//////////////////////////////////////////////////////////////////////
void DoCommand(int Command)
{
    switch(Command)
    {
        case 1: /// Adding Student
            AddStudent();
            break;
        case 2: /// Adding Passed Course
            AddPassedCourse();
            break;
        case 3: /// Adding Course
            AddCourse();            
            break;
        case 4: /// List Of Students Ordered By Lastname
            StudentOrderByLastname();
            break;
        case 5: /// List Of Students Ordered By StudentNumber
            StudentOrderByNumber();
            break;
        case 6: /// List Of Faild Students
            FaildStudents();
            break;
        case 7: /// List Of Best Students :(
            // BestStudents();
            break;
        case 8: /// List Of Students With Less Than 14 Units Passed
            StudentLess14();
            break;
        case 9: /// Rmoving Student
            RemoveStudent();
            break;
        case 10: /// Showing Student's Info
            ShowStudentInfo();
            break;
        case 11: /// Rmoving Course
            RemoveCourse();
            break;
        case 12: /// Exit
            exit(0);
            break;
        default:
            cout << "Command not found! Please type 12 to exit and <OutPutFileName> --help for list of commands." << endl;
            break;
    }
}
/////////////////////////// End of Defining Commands Function ////////////////////


int main(int argc, char **argv)
{
    if(argc > 1)
    {
        if(strcmp(argv[1],"--help") == 0)
        {
            cout << "|-------------------------------------------------|" << endl;
            cout << "|                       Help                      |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "|-------------------------------------------------|" << endl;        
            cout << "| Command   | Function                            |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 1         | Adding New Student                  |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 2         | Adding New Passed Course            |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 3         | Adding New Course                   |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 4         | Sorting Students By Lastname        |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 5         | Sorting Students By Student Number  |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 6         | List Of Failed Students             |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 7         | List Of Best Students               |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 8         | List Of Students With Less Than 14  |" << endl;
            cout << "|           | Units Passed                        |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 9         | Removing Students                   |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 10        | Showing Student's Info              |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 11        | Removing Course                     |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << "| 12        | Exit                                |" << endl;
            cout << "|-------------------------------------------------|" << endl;
            cout << endl << endl;
        }
    }

    
    char  a[5];
    int b;
    while(1)
    {
        cout << ">";        
        cin >> a;
        b = atoi(a);
        DoCommand(b);
    }
}
